import wx
from op import Shape, RedoDB, ShapeDataBase
import trimesh as tm
from io import BytesIO
import numpy as np
import wx.html as ht

class ChangeWeightDialog(wx.Dialog):

    def __init__(self, weights, funct, *args, **kw):
        super(ChangeWeightDialog, self).__init__(*args, **kw)

        self.funct = funct
        self.weights = weights

        self.InitUI()
        self.SetSize((400, 350))
        self.SetTitle("Set descriptor weights")


    def InitUI(self):

        pnl = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)

        sb = wx.StaticBox(pnl, label='Weights')
        sbs = wx.StaticBoxSizer(sb, orient=wx.VERTICAL)

        bx1 = wx.BoxSizer(orient=wx.HORIZONTAL)
        st1 = wx.StaticText(pnl, size=(200,5), label="Area")
        self.w1 = wx.TextCtrl(pnl, -1)
        self.w1.SetLabel(str(self.weights[0]))
        self.w1.Bind(wx.EVT_CHAR, self.HandleKeypress)
        bx1.Add(st1, 5, wx.EXPAND)
        bx1.Add(self.w1)
        sbs.Add(bx1)

        bx2 = wx.BoxSizer(orient=wx.HORIZONTAL)
        st2 = wx.StaticText(pnl, size=(200,5), label="Volume")
        self.w2 = wx.TextCtrl(pnl, -1)
        self.w2.SetLabel(str(self.weights[1]))
        self.w2.Bind(wx.EVT_CHAR, self.HandleKeypress)
        bx2.Add(st2, 5, wx.EXPAND)
        bx2.Add(self.w2)
        sbs.Add(bx2)

        bx10 = wx.BoxSizer(orient=wx.HORIZONTAL)
        st10 = wx.StaticText(pnl, size=(200,5), label="Sphericity")
        self.w10 = wx.TextCtrl(pnl, -1)
        self.w10.SetLabel(str(self.weights[1]))
        self.w10.Bind(wx.EVT_CHAR, self.HandleKeypress)
        bx10.Add(st10, 5, wx.EXPAND)
        bx10.Add(self.w10)
        sbs.Add(bx10)

        bx3 = wx.BoxSizer(orient=wx.HORIZONTAL)
        st3 = wx.StaticText(pnl, size=(200,5), label="Eccentricity")
        self.w3 = wx.TextCtrl(pnl, -1)
        self.w3.SetLabel(str(self.weights[2]))
        self.w3.Bind(wx.EVT_CHAR, self.HandleKeypress)
        bx3.Add(st3, 5, wx.EXPAND)
        bx3.Add(self.w3)
        sbs.Add(bx3)

        bx4 = wx.BoxSizer(orient=wx.HORIZONTAL)
        st4 = wx.StaticText(pnl, size=(200,5), label="Diameter")
        self.w4 = wx.TextCtrl(pnl, -1)
        self.w4.SetLabel(str(self.weights[3]))
        self.w4.Bind(wx.EVT_CHAR, self.HandleKeypress)
        bx4.Add(st4, 5, wx.EXPAND)
        bx4.Add(self.w4)
        sbs.Add(bx4)

        bx5 = wx.BoxSizer(orient=wx.HORIZONTAL)
        st5 = wx.StaticText(pnl, size=(200,5), label="A3")
        self.w5 = wx.TextCtrl(pnl, -1)
        self.w5.SetLabel(str(self.weights[4]))
        self.w5.Bind(wx.EVT_CHAR, self.HandleKeypress)
        bx5.Add(st5, 5, wx.EXPAND)
        bx5.Add(self.w5)
        sbs.Add(bx5)

        bx6 = wx.BoxSizer(orient=wx.HORIZONTAL)
        st6 = wx.StaticText(pnl, size=(200,5), label="D1")
        self.w6 = wx.TextCtrl(pnl, -1)
        self.w6.SetLabel(str(self.weights[5]))
        self.w6.Bind(wx.EVT_CHAR, self.HandleKeypress)
        bx6.Add(st6, 5, wx.EXPAND)
        bx6.Add(self.w6)
        sbs.Add(bx6)

        bx7 = wx.BoxSizer(orient=wx.HORIZONTAL)
        st7 = wx.StaticText(pnl, size=(200,5), label="D2")
        self.w7 = wx.TextCtrl(pnl, -1)
        self.w7.SetLabel(str(self.weights[6]))
        self.w7.Bind(wx.EVT_CHAR, self.HandleKeypress)
        bx7.Add(st7, 5, wx.EXPAND)
        bx7.Add(self.w7)
        sbs.Add(bx7)

        bx8 = wx.BoxSizer(orient=wx.HORIZONTAL)
        st8 = wx.StaticText(pnl, size=(200,5), label="D3")
        self.w8 = wx.TextCtrl(pnl, -1)
        self.w8.SetLabel(str(self.weights[7]))
        self.w8.Bind(wx.EVT_CHAR, self.HandleKeypress)
        bx8.Add(st8, 5, wx.EXPAND)
        bx8.Add(self.w8)
        sbs.Add(bx8)

        bx9 = wx.BoxSizer(orient=wx.HORIZONTAL)
        st9 = wx.StaticText(pnl, size=(200,5), label="D4")
        self.w9 = wx.TextCtrl(pnl, -1)
        self.w9.SetLabel(str(self.weights[8]))
        self.w9.Bind(wx.EVT_CHAR, self.HandleKeypress)
        bx9.Add(st9, 5, wx.EXPAND)
        bx9.Add(self.w9)
        sbs.Add(bx9)

        pnl.SetSizer(sbs)

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        okButton = wx.Button(self, label='Ok')
        closeButton = wx.Button(self, label='Close')
        hbox2.Add(okButton)
        hbox2.Add(closeButton, flag=wx.LEFT, border=5)

        vbox.Add(pnl, proportion=1,
            flag=wx.ALL|wx.EXPAND, border=5)
        vbox.Add(hbox2, flag=wx.ALIGN_CENTER|wx.TOP|wx.BOTTOM, border=10)

        self.SetSizer(vbox)

        okButton.Bind(wx.EVT_BUTTON, self.Validate)
        closeButton.Bind(wx.EVT_BUTTON, self.OnClose)


    def Validate(self, event):
        self.funct([float(self.w1.GetLineText(0)), float(self.w2.GetLineText(0)), float(self.w10.GetLineText(0)), float(self.w3.GetLineText(0)), float(self.w4.GetLineText(0)), float(self.w5.GetLineText(0)), float(self.w6.GetLineText(0)), float(self.w7.GetLineText(0)), float(self.w8.GetLineText(0)), float(self.w9.GetLineText(0))])
        self.Destroy()

    def HandleKeypress(self, event):
        keycode = event.GetKeyCode()
        if keycode < 255:
            # valid ASCII
            if chr(keycode).isnumeric() or keycode in (8, 45, 46, 127, 314, 316):
                # Valid alphanumeric character
                event.Skip()

    def OnClose(self, e):
        self.Destroy()

class DifferencesDialog(wx.Dialog):

    def __init__(self, weights, diffs, *args, **kw):
        super(DifferencesDialog, self).__init__(*args, **kw)

        self.weights = weights
        self.diffs = diffs

        self.InitUI()
        self.SetSize((350, 400))
        self.SetTitle("Differences between the current shapes")

    def InitUI(self):

        names = ['Area', "Sphericity", 'Volume', 'Eccentricity', 'Diameter', 'A3', 'D1', 'D2', 'D3', 'D4']

        pnl = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)

        sb = wx.StaticBox(pnl, label='Differences')
        sbs = wx.StaticBoxSizer(sb, orient=wx.VERTICAL)

        for n, d, w in zip(names, self.diffs, self.weights):

            bx1 = wx.BoxSizer(orient=wx.HORIZONTAL)
            st1 = wx.StaticText(pnl, size=(50,25), label=f"{n}")
            bx1.Add(st1, 1, wx.EXPAND)
            st2 = wx.StaticText(pnl, size=(100,25), label="= {0:.4f} * {1:.2f}".format(d, w))
            bx1.Add(st2, 1, wx.EXPAND)
            st3 = wx.StaticText(pnl, size=(100,25), label="= {0:.4f}".format(d*w))
            bx1.Add(st3, 1, wx.EXPAND)
            sbs.Add(bx1)

        bx1 = wx.BoxSizer(orient=wx.HORIZONTAL)
        st1 = wx.StaticText(pnl, size=(200,25), label="Total")
        bx1.Add(st1, 1, wx.EXPAND)
        st2 = wx.StaticText(pnl, size=(100,25), label="= {0:.4f}".format(np.dot(self.diffs, self.weights)))
        bx1.Add(st2, 1, wx.EXPAND)
        sbs.Add(bx1)

        pnl.SetSizer(sbs)

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        okButton = wx.Button(self, label='Ok')
        closeButton = wx.Button(self, label='Close')
        hbox2.Add(okButton)
        hbox2.Add(closeButton, flag=wx.LEFT, border=5)

        vbox.Add(pnl, proportion=1,
            flag=wx.ALL|wx.EXPAND, border=5)
        vbox.Add(hbox2, flag=wx.ALIGN_CENTER|wx.TOP|wx.BOTTOM, border=10)

        self.SetSizer(vbox)

        okButton.Bind(wx.EVT_BUTTON, self.OnClose)
        closeButton.Bind(wx.EVT_BUTTON, self.OnClose)

    def OnClose(self, e):
        self.Destroy()

class FileDrop(wx.FileDropTarget):

    def __init__(self, window, funct):
        wx.FileDropTarget.__init__(self)
        self.window = window
        self.funct = funct

    def OnDropFiles(self, x, y, filenames):

        for name in filenames:

            try:
                self.funct(name)

            except IOError as error:

                msg = "Error opening file\n {}".format(str(error))
                dlg = wx.MessageDialog(None, msg)
                dlg.ShowModal()

                return False

            except UnicodeDecodeError as error:

                msg = "Cannot open non ascii files\n {}".format(str(error))
                dlg = wx.MessageDialog(None, msg)
                dlg.ShowModal()

                return False

        return True

class Example(wx.Frame):

    def __init__(self, parent, title):
        super(Example, self).__init__(parent, title=title)
        self.SetSize(1520, 980)
        self.SetMinSize(self.Size)
        self.SetMaxSize(self.Size)
        self.database = None
        self.InitUI()
        self.Centre()
        self.current_image = None
        self.match_results = None
        self.outside = False
        self.match_index = 0
        self.weights = [1.,1.,1.,1.,1.,1.,1.,1.,1.,1.,]

    def manage_mesh_file(self, path):
        shape = Shape(tm.load_mesh(path), None)
        shape.Refine(self.database.mean_v)
        shape.Normalize()
        shape.CalcShapeDescriptors()
        shape.DescNormalize(self.database.norm_mins, self.database.norm_maxs)
        shape.FixHistograms(self.database.mins, self.database.maxs)
        shape.SaveAndRemove(self.database.mdir)
        self.selected_shape = shape

    def InitUI(self):

        '''menubar = wx.MenuBar()
        fileMenu = wx.Menu()
        menubar.Append(fileMenu, '&File')
        self.SetMenuBar(menubar)'''

        vbox = wx.BoxSizer(wx.VERTICAL)
        self.Bind(wx.EVT_CHAR_HOOK, self.ArrowPress)

        gs = wx.FlexGridSizer(1, 3, 5, 5)

        bx = wx.BoxSizer(orient=wx.VERTICAL)
        bxbutt1 = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.fb = wx.Button(self, label = "Open file")
        def mfp(path):
            self.setSelectedImage(path)
            self.meshFilePick.SetValue(path)
        self.fb.Bind(wx.EVT_BUTTON, lambda e: self.OpenMesh(e, mfp))
        self.fb.Disable()
        self.meshFilePick = wx.TextCtrl(self, style=wx.TE_READONLY,value="Drag .OFF or .PLY file")
        dt = FileDrop(self.meshFilePick, mfp)
        self.meshFilePick.SetDropTarget(dt)
        bxbutt1.Add(self.fb, 0, wx.EXPAND)
        bxbutt1.AddSpacer(3)
        bxbutt1.Add(self.meshFilePick, 1, wx.EXPAND)

        bxbutt2 = wx.BoxSizer(orient=wx.HORIZONTAL)
        fb1 = wx.Button(self, label = "Open file")
        def dfp(path):
            fb1.Disable()
            self.database = np.load(path, allow_pickle=True)[0]
            self.cb1.Clear()
            self.cb1.InsertItems(np.unique([x.sclass for x in self.database]), 0)
            fb1.Enable()
        fb1.Bind(wx.EVT_BUTTON, lambda e: self.OpenNPY(e, dfp))
        self.dbFilePick = wx.TextCtrl(self, style=wx.TE_READONLY,value="Drag .NPY file to load previously saved database")
        dt = FileDrop(self.dbFilePick, dfp)
        self.dbFilePick.SetDropTarget(dt)
        bxbutt2.Add(fb1, 0, wx.EXPAND)
        bxbutt2.AddSpacer(3)
        bxbutt2.Add(self.dbFilePick, 1, wx.EXPAND)

        bxbutt3 = wx.BoxSizer(orient=wx.HORIZONTAL)
        fb2 = wx.Button(self, label = "Open folder")
        def dflp(path):
            fb2.Disable()
            self.database = RedoDB(path, resolution = (self.png_selected.Size.Width, self.png_selected.Size.Height), save=False)
            self.cb1.Clear()
            self.cb1.InsertItems(np.unique([x.sclass for x in self.database]), 0)
            fb2.Enable()
        fb2.Bind(wx.EVT_BUTTON, lambda e: self.OpenFolder(e, dflp))
        self.dbFolderPick = wx.TextCtrl(self, style=wx.TE_READONLY,value="Select a folder to create a meshes database",size=(250,fb1.Size.Height))
        #dt = FileDrop(self.dbFolderPick, dflp)
        #self.dbFolderPick.SetDropTarget(dt)
        svButt = wx.Button(self, label="Save DB into NPY file")
        svButt.Bind(wx.EVT_BUTTON, self.saveDB)
        bxbutt3.Add(fb2, 0, wx.EXPAND)
        bxbutt3.AddSpacer(3)
        bxbutt3.Add(self.dbFolderPick, 1, wx.EXPAND)
        bxbutt3.AddSpacer(3)
        bxbutt3.Add(svButt, 0, wx.EXPAND)
        gs2 = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.cb1 = wx.ListBox(self, choices=[])
        self.cb1.Bind(wx.EVT_LISTBOX, self.onList1Box)
        self.cb2 = wx.ListBox(self, choices=[])
        self.cb2.Bind(wx.EVT_LISTBOX, self.onList2Box)
        gs2.AddMany(
            [
                (self.cb1, 3, wx.EXPAND),
                (self.cb2, 3, wx.EXPAND)
            ]
        )

        self.btdr = wx.Button(self, label = "Show 2D t-SNE")
        self.btdr.Bind(wx.EVT_BUTTON, self.ShowTSNE)
        self.btdr.Disable()
        self.btdr3 = wx.Button(self, label = "Show 3D t-SNE")
        self.btdr3.Bind(wx.EVT_BUTTON, self.ShowTSNE3D)
        self.btdr3.Disable()
        bx.AddMany([(bxbutt1, 0, wx.EXPAND), (bxbutt2, 0, wx.EXPAND), (bxbutt3, 0, wx.EXPAND), (gs2, 1, wx.EXPAND), (self.btdr, 0, wx.EXPAND), (self.btdr3, 0, wx.EXPAND)])

        bx2 = wx.BoxSizer(orient=wx.VERTICAL)
        self.png_selected = wx.StaticBitmap(self, bitmap=wx.Bitmap(wx.Image(660, int(self.Size.Height // 2.42))))
        bx2.Add(self.png_selected, 0, wx.EXPAND, 5)
        bt3d1 = wx.Button(self, label = "Show 3D view")
        bt3d1.Bind(wx.EVT_BUTTON, self.Show3DSelected)
        bx2.Add(bt3d1, 0, wx.EXPAND, 5)
        bx2.AddSpacer(15) 
        bxctr = wx.BoxSizer(orient=wx.HORIZONTAL)
        lbl = wx.StaticText(self,label="Number of results") 
        bxctr.Add(lbl, 0, wx.EXPAND, 5)
        bxctr.AddSpacer(5) 
        self.nres = wx.TextCtrl(self, -1)
        self.nres.SetLabel(str(20))
        self.nres.Bind(wx.EVT_CHAR, self.HandleKeypress)
        bxctr.Add(self.nres, 0, wx.EXPAND, 5)
        bxctr.AddSpacer(5) 
        self.btsch = wx.Button(self, label = "Search closest mesh")
        self.btsch.Bind(wx.EVT_BUTTON, self.Search)
        self.btsch.Disable()
        bxctr.Add(self.btsch, 1, wx.EXPAND, 5)
        self.btann = wx.Button(self, label = "Search using ANN")
        self.btann.Bind(wx.EVT_BUTTON, self.SearchANN)
        self.btann.Disable()
        bxctr.Add(self.btann, 1, wx.EXPAND, 5)
        bx2.Add(bxctr, 0, wx.EXPAND, 5)
        bx2.AddSpacer(15) 
        bx_sub1 = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.png_result = wx.StaticBitmap(self, bitmap=wx.Bitmap(wx.Image(660, int(self.Size.Height // 2.42))))
        self.png_result.Bind(wx.EVT_LEFT_DOWN, self.ClickImage)
        bx2.Add(self.png_result, 0, wx.EXPAND, 5)
        bt3d2 = wx.Button(self, label = "Show 3D view")
        bt3d2.Bind(wx.EVT_BUTTON, self.Show3DResult)
        bx2.Add(bt3d2, 0, wx.EXPAND, 5)

        #gs3 = wx.GridSizer(2,1,0,0)
        gs3 = wx.BoxSizer(orient=wx.VERTICAL)
        bx_sub1 = wx.BoxSizer(orient=wx.VERTICAL)
        self.tx_desc1 = wx.StaticText(self,size=(300, 75))
        bx_sub1.Add(self.tx_desc1, 0, wx.EXPAND, 5)
        self.hists_sel = wx.StaticBitmap(self, bitmap=wx.Bitmap(wx.Image(400, self.Size.Height // 3)))
        bx_sub1.Add(self.hists_sel, 0, wx.EXPAND, 0)
        gs3.Add(bx_sub1, 5, wx.EXPAND)
        gs3.AddSpacer(10) 
        wdbts = wx.BoxSizer(orient=wx.HORIZONTAL)
        wButt = wx.Button(self, label = "Change descriptor weights", size=(200,26))
        wButt.Bind(wx.EVT_BUTTON, self.WeightDialog)
        wdbts.Add(wButt, 1, 0)
        self.dButt = wx.Button(self, label = "Show descriptor differences", size=(200,26))
        self.dButt.Bind(wx.EVT_BUTTON, self.DifferencesDialog)
        self.dButt.Disable()
        wdbts.Add(self.dButt, 1, 0)
        gs3.Add(wdbts, 0, wx.EXPAND)
        gs3.AddSpacer(10) 
        bx_sub2 = wx.BoxSizer(orient=wx.VERTICAL)
        self.tx_desc2 = wx.StaticText(self,size=(300, 75))
        bx_sub2.Add(self.tx_desc2, 0, wx.EXPAND, 5)
        self.hists_res = wx.StaticBitmap(self, bitmap=wx.Bitmap(wx.Image(400, self.Size.Height // 3)))
        bx_sub2.Add(self.hists_res, 0, wx.EXPAND, 0)
        gs3.Add(bx_sub2, 5, wx.EXPAND)

        gs.AddMany([(bx, 5, wx.EXPAND), (bx2, 5, wx.EXPAND), (gs3, 5, wx.EXPAND)])
        gs.AddGrowableCol(1,1)

        '''gs.AddMany( [(wx.Button(self, label='Cls'), 0, wx.EXPAND),
            (wx.Button(self, label='Bck'), 0, wx.EXPAND),
            (wx.StaticText(self), wx.EXPAND),
            (wx.Button(self, label='Close'), 0, wx.EXPAND),
            (wx.Button(self, label='7'), 0, wx.EXPAND),
            (wx.Button(self, label='8'), 0, wx.EXPAND),
            (wx.Button(self, label='9'), 0, wx.EXPAND),
            (wx.Button(self, label='/'), 0, wx.EXPAND),
            (wx.Button(self, label='4'), 0, wx.EXPAND),
            (wx.Button(self, label='5'), 0, wx.EXPAND),
            (wx.Button(self, label='6'), 0, wx.EXPAND),
            (wx.Button(self, label='*'), 0, wx.EXPAND),
            (wx.Button(self, label='1'), 0, wx.EXPAND),
            (wx.Button(self, label='2'), 0, wx.EXPAND),
            (wx.Button(self, label='3'), 0, wx.EXPAND),
            (wx.Button(self, label='-'), 0, wx.EXPAND),
            (wx.Button(self, label='0'), 0, wx.EXPAND),
            (wx.Button(self, label='.'), 0, wx.EXPAND),
            (wx.Button(self, label='='), 0, wx.EXPAND),
            (wx.Button(self, label='+'), 0, wx.EXPAND) ])'''

        vbox.Add(gs, proportion=1, flag=wx.EXPAND)
        self.SetSizer(vbox)

    def onList1Box(self, event): 
        self.cb2.Clear()
        self.cb2.InsertItems([x.name for x in self.database if x.sclass == event.GetEventObject().GetStringSelection()],0)

    def onList2Box(self, event):
        self.selected_shape = next(x for x in self.database if x.name == event.GetEventObject().GetStringSelection())
        png = self.selected_shape.GetFigure(self.png_selected.Size.Width, self.png_selected.Size.Height)

        self.tx_desc1.SetLabel(f'Area: {self.selected_shape.area:26.10f}\nSphericity: {self.selected_shape.sphe:16.10f}\nVolume: {self.selected_shape.vol:21.10f}\nDiameter: {self.selected_shape.dia:18.10f}\nEccentricity: {self.selected_shape.ecc:14.10f}')
        
        self.hists_sel.SetBitmap(wx.Bitmap(wx.Image(self.selected_shape.GetPNGHistograms([350/96, (self.Size.Height // 3.1) / 96]))))
        self.png_selected.SetBitmap(wx.Bitmap(wx.Image(png)))
        self.btsch.Enable()
        self.btann.Enable()
        self.outside = False

    def setSelectedImage(self, path):
        try:
            self.manage_mesh_file(path)
            try:
                png = self.selected_shape.GetFigure(self.png_selected.Size.Width, self.png_selected.Size.Height)
            except:
                png = self.selected_shape.GetFigure(self.png_selected.Size.Width, self.png_selected.Size.Height, force = True)
            self.tx_desc1.SetLabel(f'Area: {self.selected_shape.area:26.10f}\nSphericity: {self.selected_shape.sphe:16.10f}\nVolume: {self.selected_shape.vol:21.10f}\nDiameter: {self.selected_shape.dia:18.10f}\nEccentricity: {self.selected_shape.ecc:14.10f}')
        
            self.hists_sel.SetBitmap(wx.Bitmap(wx.Image(self.selected_shape.GetPNGHistograms([350/96, (self.Size.Height // 3.1) / 96]))))
            self.png_selected.SetBitmap(wx.Bitmap(wx.Image(png)))
            self.btsch.Enable()
            self.btann.Enable()
        except IOError:
            wx.LogError("Cannot open file '%s'." % path)

    def Show3DSelected(self, e):
        self.selected_shape.Show()

    def ShowHistSelected(self, e):
        self.selected_shape.ShowHistograms()

    def Show3DResult(self, e):
        self.result_shape.Show()

    def ShowHistResult(self, e):
        self.result_shape.ShowHistograms()

    def LoadResult(self):
        png = self.result_shape.GetFigure(self.png_result.Size.Width, self.png_result.Size.Height)
        
        self.tx_desc2.SetLabel(f'Area: {self.result_shape.area:26.10f}\nSphericity: {self.selected_shape.sphe:16.10f}\nVolume: {self.result_shape.vol:21.10f}\nDiameter: {self.result_shape.dia:18.10f}\nEccentricity: {self.result_shape.ecc:14.10f}')
        
        self.hists_res.SetBitmap(wx.Bitmap(wx.Image(self.result_shape.GetPNGHistograms([350/96, (self.Size.Height // 3.1) / 96]))))
            
        self.png_result.SetBitmap(wx.Bitmap(wx.Image(png)))

    def Search(self, e):
        self.res_indices = self.database.ComputeDifferences(self.selected_shape, self.weights)[:int(self.nres.GetLineText(0))]
        self.match_index = 0
        self.result_shape = self.database[self.res_indices[0]]
        self.LoadResult()
        self.dButt.Enable()

    def SearchANN(self, e):
        if not hasattr(self.database, 'ann'):
            self.database.BuildANN()
        self.res_indices = self.database.ANNSearch(self.selected_shape, int(self.nres.GetLineText(0))) if not self.outside else self.database.ANNSearchV(self.selected_shape.GetDescriptors(), int(self.nres.GetLineText(0)))
        self.match_index = 0
        self.result_shape = self.database[self.res_indices[0]]
        self.LoadResult()
        self.dButt.Enable()

    def WeightDialog(self, e):
        wDialog = ChangeWeightDialog(self.weights, self.ChangeWeights, None, title="Change Weights")
        wDialog.ShowModal()
        wDialog.Destroy()

    def DifferencesDialog(self, e):
        dDialog = DifferencesDialog(self.weights, self.selected_shape.Difference(self.result_shape), None, title="Descriptor differences")
        dDialog.ShowModal()
        dDialog.Destroy()

    def HandleKeypress(self, event):
        keycode = event.GetKeyCode()
        print(keycode)
        if keycode < 255:
            # valid ASCII
            if chr(keycode).isnumeric() or keycode in (8, 127, 314, 316):
                # Valid alphanumeric character
                event.Skip()

    def ChangeWeights(self, weights):
        self.weights = weights

    def ArrowPress(self, e):
        kc = e.GetKeyCode()
        if hasattr(self, 'result_shape') and kc in [314, 316]:
            self.match_index += 1 if kc == 316 else -1
            self.result_shape = self.database[self.res_indices[self.match_index%len(self.res_indices)]]
            self.LoadResult()

    def ClickImage(self, e):
        x, y = e.GetPosition()
        self.match_index += 1 if x > 340 else -1
        self.result_shape = self.database[self.res_indices[self.match_index%len(self.res_indices)]]
        self.LoadResult()
        
    def OpenMesh(self, e, func):
        with wx.FileDialog(self, "Open mesh  file", wildcard="OFF files (*.off)|*.off|PLY files (*.ply)|*.ply|OBJ files (*.obj)|*.obj|Any file (*.*)|*.*",
                       style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind

            # Proceed loading the file chosen by the user
            pathname = fileDialog.GetPath()
            msg = "Processing shape, please wait..."
            busyDlg = wx.BusyInfo(msg)
            func(pathname)
            busyDlg = None
            self.outside = True

    def ShowTSNE(self, e):
        self.database.ShowTSNEReduction()

    def ShowTSNE3D(self, e):
        self.database.ShowTSNEReduction3D()

    def OpenNPY(self, e, func):
        with wx.FileDialog(self, "Open NPY file", wildcard="NPY files (*.npy)|*.npy",
                    style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind

            # Proceed loading the file chosen by the user
            pathname = fileDialog.GetPath()
            msg = "Loading the database, please wait..."
            busyDlg = wx.BusyInfo(msg)
            func(pathname)
            self.fb.Enable()
            
            self.btdr.Enable()
            self.btdr3.Enable()
            busyDlg = None

    def OpenFolder(self, e, func):
        with wx.DirDialog(self, "Open DB Folder", style=wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST) as dirDialog:

            if dirDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind

            # Proceed loading the file chosen by the user
            pathname = dirDialog.GetPath()
            func(pathname)
            self.fb.Enable()
            self.btdr.Enable()
            self.btdr3.Enable()
        
    def saveDB(self, e):
        self.database.Save("gui-database")

    def ChangeResult(self, direction):
        self.match_index += direction
        self.result_shape = self.database[self.match_results[self.match_index]]
        png =  self.result_shape.GetUnNormalized(self.png_result.Size.Width, self.png_result.Size.Height)
        self.png_result.SetBitmap(wx.Bitmap(wx.Image(png)))

def main():

    app = wx.App()
    ex = Example(None, title='3D Mesh Searcher')
    ex.Show()
    app.MainLoop()


if __name__ == '__main__':
    main()