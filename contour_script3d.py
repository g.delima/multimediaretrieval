from op import Shape, ShapeDataBase, RedoDb
import numpy as np
import trimesh as tm
import time
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.stats import mode
from math import ceil


colours = ['#1F0A51', '#0D1061', '#113470', '#145F7E', '#198C88', '#1D9A6C', '#31B358', '#4EC849', '#90DA6A', '#CAE88D', '#F3F0B0']

#db = np.load("CDB.npy", allow_pickle = True)[0]
db = RedoDb('./LabeledDB', './Meshes', save=False)

def truncate(n, decimals=0):
    multiplier = 10 ** decimals
    return int(n * multiplier) / multiplier

def round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return ceil(n * multiplier) / multiplier

def createContour(vals, labels):
    levels = np.linspace(truncate(np.min(vals),1), round_up(np.max(vals),1),10)
    cs = plt.contourf(vals, levels = levels)
    plt.xticks(range(len(labels)), labels = labels)
    proxy = [plt.Rectangle((0,0),1,1,fc = pc.get_facecolor()[0]) for pc in cs.collections]
    plt.legend(proxy, levels, bbox_to_anchor=(1.1, 1))
    plt.show()

def createContour3D(X, Y, Z, cols, ticks):

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    X, Y = np.meshgrid(X, Y)
    surf = ax.plot_surface(X, Y, Z, facecolors=cols, linewidth=0)

    plt.yticks(range(len(ticks)), labels = ticks)
    proxy = [plt.Rectangle((0,0),1,1,fc = c) for c in colours]
    plt.legend(proxy, np.arange(11)/10)
    #plt.legend(proxy, range(11)/10, bbox_to_anchor=(1.1, 1))
    plt.show()


classes = np.unique([s.sclass for s in db])
els = np.sum([1 for s in db if s.sclass == db[0].sclass])
bins = 7
ppv = np.empty((len(classes), els, bins)) 
tnr = np.empty((len(classes), els, bins))
stts = np.zeros((len(classes), 4, bins),dtype = np.longdouble)
for b in range(bins):
    db.ReBinHistograms(2**(b+3))
    c_count = {c:0 for c in classes}
    for s in db:
        lst = db.ComputeDifferences(s)
        #ind = next(i for i, x in enumerate(lst[::-1]) if db[x].sclass == s.sclass)
        S = db[lst[:els]]
        N = db[lst[els:]] 
        tp = sum([1 for x in S if x.sclass == s.sclass])
        tn = sum([1 for x in N if not x.sclass == s.sclass])

        cs = np.argwhere(classes==s.sclass)[0][0]
        ppv[cs, c_count[s.sclass], b] = tp / len(S) if len(S) > 0 else 0
        tnr[cs, c_count[s.sclass], b] = tn / (tn + len(S) - tp)
        stts[cs, :, b] += [tp, len(S)-tp, tn, len(N)-tn]
        c_count[s.sclass]+=1

Z1 = np.argmax(ppv, axis=2)+3
Z2 = np.argmax(tnr, axis=2)+3

col1 = [list(map(lambda x: colours[int(x*10)], x)) for x in np.max(ppv, axis=2)]
col2 = [list(map(lambda x: colours[int(x*10)], x)) for x in np.max(tnr, axis=2)]

print(f'Mode for PPV: {mode(Z1, axis = None)}')
print(f'Mode for TNR: {mode(Z2, axis = None)}')

createContour3D(np.arange(els), np.arange(len(classes)), Z1, col1, classes)
createContour3D(np.arange(els), np.arange(len(classes)), Z2, col2, classes)


tps = stts[:,0,:]
fps = stts[:,1,:]
tns = stts[:,2,:]
fns = stts[:,3,:]
mcc = np.multiply(np.multiply(tps, tns)+np.multiply(fps, fns), 1/np.sqrt(np.multiply(np.multiply(np.multiply(tps+fps, tps+fns),tns+fps),tns+fns)))

ppvs = np.multiply(tps, 1/(tps+fps))
tnrs = np.multiply(tns, 1/(tns+fps))

createContour(mcc.T, classes)
createContour(ppvs.T, classes)
createContour(tnrs.T, classes)
db.SetBinHistograms(int(input()))
db.Save('CDB')
x = 0