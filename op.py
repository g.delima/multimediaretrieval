import os 
import math
import sys
import numpy as np
import trimesh as tm
from PIL import Image
import matplotlib.pyplot as plt
import progressbar
import time
from io import BytesIO
from sklearn.decomposition import PCA
from scipy.stats import wasserstein_distance as emdist
import trimesh.viewer as vw
from hashlib import md5
import gc
import logging
from annoy import AnnoyIndex
from sklearn.manifold import TSNE
from itertools import accumulate

class Shape():
    def __init__(self, mesh, name, sclass = None):
        self.mesh = mesh
        self.name = name
        self.sclass = sclass
        self.vertices = len(mesh.vertices)
        self.faces = len(mesh.faces)
        self.stype = set(list(map(len,mesh.faces)))

    def Refine(self, val):
        if len(self.mesh.vertices) < val:
            while len(self.mesh.vertices) < val:
                self.mesh = self.mesh.subdivide()
            self.mesh = tm.smoothing.filter_humphrey(self.mesh)
        self.vertices = len(self.mesh.vertices)
        self.faces = len(self.mesh.faces)
    
    def Normalize(self):
        self.mesh = self.mesh.apply_translation(-self.mesh.centroid)

        self.pca = PCA()
        self.pca.fit(self.mesh.vertices)
        rt = np.eye(4)
        rt[:3,:3] = self.pca.components_
        self.mesh.apply_transform(rt)
        
        flp = np.eye(4)
        points = self.mesh.vertices[self.mesh.faces]
        np.fill_diagonal(flp, np.sign(np.sum(np.multiply(np.sign(points), np.multiply(points, points)), axis = (0,1))))
        self.mesh.apply_transform(flp)

        self.mesh = self.mesh.apply_scale(1 / max(self.mesh.extents))

    def CalcAng(self, p):
        ba = p[0] - p[1]
        bc = p[2] - p[1]

        cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
        return np.arccos(np.clip(cosine_angle, -1, 1))

    def CalcArea(self, p):
        return 0.5 * np.linalg.norm(np.cross(p[1]-p[0], p[2]-p[0]))

    def CalcVol(self, p):
        return np.linalg.norm(np.dot(p[0]-p[3], np.cross(p[1]-p[3],p[2]-p[3])))/6

    def CalcShapeDescriptors(self):
        n = 2*self.vertices
        x = np.float32(np.sort(np.random.rand(n, 2), axis=0))
        p_c = np.squeeze([np.column_stack([z[0], z[1]-z[0], 1.0-z[1]]) @ v
                            for z, v in zip(x,
                                np.float32(self.mesh.triangles[
                                    np.random.choice(
                                        range(self.faces), size=(n,), p=self.mesh.area_faces / sum(self.mesh.area_faces)
                                    )
                                ])
                            )
                        ]) #This is the bottleneck, if I could vectorize this, calculating shape descriptors would be a matter of milliseconds
                        # Doing self.mesh.triangles[np.random.choice(range(self.faces), size=(n,5), p=self.mesh.area_faces / sum(self.mesh.area_faces)) speeds up the process considerably, but requires too much memory
        self.area = self.mesh.area
        self.vol = self.mesh.bounding_box.volume
        self.sphe = np.pi**(1/3)*(6*self.vol)**(2/3)/self.area
        self.ecc = max(self.pca.explained_variance_) / min(self.pca.explained_variance_)

        aux1 = p_c[:-2,:]-p_c[1:-1,:]
        aux2 = p_c[2:,:]-p_c[1:-1,:]
        self.A3 = np.arccos(np.clip(np.einsum('ij,ij->i', aux1, aux2) / np.multiply(np.linalg.norm(aux1, axis=1), np.linalg.norm(aux2, axis=1)),-1,1))

        self.D1 = np.linalg.norm(p_c[:,:] - self.mesh.centroid, axis = 1)

        self.D2 = np.linalg.norm((p_c[:-1].T- p_c[1:].T).T, axis = 1)
        self.dia = max(self.D2)

        aux1 = p_c[1:-1,:]-p_c[:-2,:]
        aux2 = p_c[2:,:]-p_c[:-2,:]
        self.D3 = np.sqrt(np.linalg.norm(np.cross(aux1, aux2), axis = 1))

        aux1 = p_c[:-3,:]-p_c[3:,: ]
        aux2 = p_c[1:-2,:]-p_c[3:,:]
        aux3 = p_c[2:-1,:]-p_c[3:,:]
        self.D4 = np.abs(np.einsum('ij,ij->i', aux1, np.cross(aux2, aux3))) / 6 #np.einsum('ij,ij->i') computes the row-wise dot product of two matrices
        
        del aux1, aux2, aux3
        return [self.area, self.sphe, self.vol, self.ecc, self.dia], [[min(self.A3), max(self.A3)], [min(self.D1), max(self.D1)], [min(self.D2), max(self.D2)], [min(self.D3), max(self.D3)], [min(self.D4), max(self.D4)]]
    
    def DescNormalize(self, mins, maxs):
        self.area, self.sphe, self.vol, self.ecc, self.dia = ([self.area, self.sphe, self.vol, self.ecc, self.dia] - mins) / (maxs-mins)

    def CreateHistograms(self, mins, maxs, bins = 10):
        self.histograms = []
        for d, mn, mx in zip([self.A3, self.D1, self.D2, self.D3, self.D4], mins, maxs):
            h = np.histogram(d, range=(mn, mx), bins = bins)
            self.histograms.append((h[0] / sum(h[0]), h[1]))

    def FixHistograms(self, mins, maxs, bins=10):
        self.histograms = []
        for d, mn, mx in zip([self.A3, self.D1, self.D2, self.D3, self.D4], mins, maxs):
            h = np.histogram(d, range=(mn, mx), bins = bins)
            self.histograms.append((h[0] / sum(h[0]), h[1]))
        del self.A3, self.D1, self.D2, self.D3, self.D4

    def GetDescriptors(self):
        return np.append([self.area, self.sphe, self.vol, self.ecc, self.dia], [h[0] for h in self.histograms])

    def ShowHistograms(self):
        subplots = [321, 322, 323, 324, 325]
        names = ['A3', 'D1', 'D2', 'D3', 'D4']
        for i, h in enumerate(self.histograms):
            ax = plt.subplot(subplots[i])
            ax.bar(h[1][:-1], h[0], width=(h[1][1] - h[1][0])*0.85)
            ax.set_title(names[i])
        plt.show()
        plt.clf()

    def GetPNGHistograms(self, size = [6.4, 6.4]):
        fig = plt.figure(figsize=size)
        axs = fig.subplots(nrows = len(self.histograms), ncols = 1)
        names = ['A3', 'D1', 'D2', 'D3', 'D4']
        for i, h in enumerate(self.histograms):
            axs[i].bar(h[1][:-1], h[0], width=(h[1][1] - h[1][0])*0.85)
            axs[i].set_title(names[i])
        buf = BytesIO()
        fig.savefig(buf, format='png')
        buf.seek(0)
        plt.close(fig)
        return buf

    def ShowUnNormalized(self):
        rt = np.zeros((4,4))
        rt[:3,:3] = self.pca.components_
        rt[3,3] = 1
        self.mesh.copy().apply_transform(np.linalg.inv(rt)).show()

    def GetUnNormalized(self, width, height):
        rt = np.zeros((4,4))
        rt[:3,:3] = self.pca.components_
        rt[3,3] = 1
        cp_aux = self.mesh.copy()
        cp_aux.apply_transform(np.linalg.inv(rt))
        return BytesIO(cp_aux.scene().save_image(resolution=(width, height)))

    def GetFigure(self, width, height, force=False):
        if force or not hasattr(self, 'figure'):
            mesh = tm.load_mesh(self.file)
            self.figure = BytesIO(mesh.scene().save_image(resolution=(width, height)))
        self.figure.seek(0)
        return self.figure
    
    def GetHTML(self):
        return vw.scene_to_html(self.mesh.scene())

    def Show(self):
        mesh = tm.load_mesh(self.file)
        mesh.show()

    def Reread(self):
        self.mesh = tm.load_mesh(self.file)

    def SaveAndRemove(self, location="./meshes"):
        code = md5( bytes( f'{self.sclass}/{self.name}' , 'utf-8')).hexdigest()
        self.file = f'{location}/{code}.stl'
        self.mesh.export(self.file)
        del self.mesh

    def Finalize(self, resolution =(660, 405), location="./meshes"):
        try:
            self.figure = BytesIO(self.mesh.scene().save_image(resolution=resolution))
        except Exception as e:
            print("Something went wrong with the picturing")
        code = md5( bytes( f'{self.sclass}/{self.name}' , 'utf-8')).hexdigest()
        self.file = f'{location}/{code}.stl'
        self.mesh.export(self.file)
        del self.mesh
        del self.pca
        gc.collect()


    def Get2D(self):
        cp = self.mesh.copy()
        cp.visual.face_colors[:] = [0,0,0,255]
        sc = cp.scene()
        sc.lights = []
        return BytesIO(sc.save_image())

    def Difference(self, shape):
        def l2(a,b):
            return np.sqrt(np.abs(a**2-b**2))
        res = [l2(self.area, shape.area), l2(self.sphe, shape.sphe), l2(self.vol, shape.vol), l2(self.dia, shape.dia), l2(self.ecc, shape.ecc)]
        res.extend([emdist(h1[0],h2[0]) for h1, h2 in zip(self.histograms, shape.histograms)])
        return res

    def SDifference(self, shape):
        def l2(a,b):
            return np.sign(a-b)*np.sqrt(np.abs(a**2-b**2))
        res = [l2(self.area, shape.area), l2(self.sphe, shape.sphe), l2(self.vol, shape.vol), l2(self.dia, shape.dia), l2(self.ecc, shape.ecc)]
        res.extend([np.sign(np.mean(h1[0]-h2[0]))*emdist(h1[0],h2[0]) for h1, h2 in zip(self.histograms, shape.histograms)])
        return res

class ShapeDataBase():
    def __init__(self, path, mdir = "./meshes", formats = ['.off', '.ply']):
        self.shapes = []
        self.mdir = mdir
        print("Constructing Database: ")
        i = 0
        with progressbar.ProgressBar(max_value=progressbar.UnknownLength, widgets_kwargs = progressbar.widgets.Counter) as bar:
            for subdir, dirs, files in os.walk(path):
                for f in files:
                    if f[-4:] in formats:
                        try:
                            ms = tm.load_mesh(f'{subdir}/{f}')
                            self.shapes.append(Shape(ms, f, subdir.split("\\")[-1]))
                            bar.update(i)
                            i+=1
                        except:
                            pass
        self.shapes = self.shapes
        self.sclass = np.array([s.sclass for s in self])
        self.classes = np.unique(self.sclass)
        self.mean_v = np.mean([m.vertices for m in self.shapes])
        self.median_v = np.median([m.vertices for m in self.shapes])
        self.std_v = np.std([m.vertices for m in self.shapes])
        self.pc_ws = [0.52016785, 1.66629163, 0.23627294, 0.73237672, 0.96607131, 1.03017593, 0.7781933, 0.69369216, 0.55133754, 1.72782003] 


    def Refine(self):
        s_db = sorted(self.shapes, key=lambda x: x.vertices)
        cond = self.median_v
        print("Refining: ")
        with progressbar.ProgressBar(max_value=len([1 for s in self.shapes if s.vertices < cond])) as bar:
            for i, m in enumerate(s_db):
                if m.vertices < cond:
                    m.Refine(cond)
                else:
                    break
                bar.update(i)
        self.shapes = np.array(sorted(s_db, key = lambda x: (x.sclass, x.name)))
        self.mean_v = np.mean([m.vertices for m in self.shapes])
        self.median_v = np.median([m.vertices for m in self.shapes])
        self.std_v = np.std([m.vertices for m in self.shapes])

    def Normalize(self):
        print("Normalising: ")
        with progressbar.ProgressBar(max_value=len(self.shapes)) as bar:
            for i, m in enumerate(self.shapes):
                bar.update(i)

    def RefineNormalize(self):
        s_db = sorted(self.shapes, key=lambda x: x.vertices)
        cond = self.median_v
        logging.getLogger(tm.__name__).setLevel(logging.ERROR)
        if not os.path.isdir(self.mdir):
                os.mkdir(self.mdir)
        print("Refining and normalizing: ")
        with progressbar.ProgressBar(max_value=len(self.shapes)) as bar:
            for i, m in enumerate(s_db):
                if m.vertices < cond:
                    m.Refine(cond)
                m.Normalize()
                m.SaveAndRemove(self.mdir)
                bar.update(i)
        self.shapes = np.array(sorted(s_db, key = lambda x: (x.sclass, x.name)))
        self.mean_v = np.mean([m.vertices for m in self.shapes])
        self.median_v = np.median([m.vertices for m in self.shapes])
        self.std_v = np.std([m.vertices for m in self.shapes])

    def CalcDescriptors(self):
        print("Computing shape descriptors: ")
        normalized = []
        bounds = []
        with progressbar.ProgressBar(max_value=len(self.shapes)) as bar:
            for i, m in enumerate(self.shapes):
                m.Reread()
                disc, cont = m.CalcShapeDescriptors()
                m.SaveAndRemove(self.mdir)
                normalized.append(disc)
                bounds.append(cont)
                bar.update(i)
        self.norm_mins = np.min(normalized, axis = 0)
        self.norm_maxs = np.max(normalized, axis = 0)
        bounds = np.array(bounds)
        self.mins = np.min(bounds[:,:,0], axis=0)
        self.maxs = np.max(bounds[:,:,1], axis=0)
        print("Creating histograms: ")
        with progressbar.ProgressBar(max_value=len(self.shapes)) as bar:
            for i, m in enumerate(self.shapes):
                m.DescNormalize(self.norm_mins, self.norm_maxs)
                m.FixHistograms(self.mins, self.maxs)
                bar.update(i)
    
    def Finalize(self, resolution = (660, 405)):
        if not os.path.isdir(self.mdir):
                os.mkdir(self.mdir)
        with progressbar.ProgressBar(max_value=len(self.shapes)) as bar:
            for i, m in enumerate(self.shapes):
                m.Finalize(resolution = resolution, location = self.mdir)
                bar.update(i)

    def ReBinHistograms(self, bins):
        with progressbar.ProgressBar(max_value=len(self.shapes)) as bar:
            for i, m in enumerate(self.shapes):
                m.CreateHistograms(self.mins, self.maxs, bins)
                bar.update(i)

    def SetBinHistograms(self, bins):
        with progressbar.ProgressBar(max_value=len(self.shapes)) as bar:
            for i, m in enumerate(self.shapes):
                m.FixHistograms(self.mins, self.maxs, bins)
                bar.update(i)
    
    def CompareHistograms(self, descriptor, ranges = None):
        dscs = {'A3': 0, 'D1': 1, 'D2': 2, 'D3': 3, 'D4': 4}
        ind = dscs[descriptor]
        if ranges is None:
            ranges = list(range(len(self.shapes)))
        for s in self.shapes[ranges]:
            plt.plot(s.histograms[ind][1][:-1], s.histograms[ind][0])
        plt.title(descriptor)
        plt.show()

    def CompareHistogramsComplete(self, descriptor):
        dscs = {'A3': 0, 'D1': 1, 'D2': 2, 'D3': 3, 'D4': 4}
        ind = dscs[descriptor]
        means = dict()
        for s in self.shapes:
            m_aux = means.get(s.sclass, ([0]*10, [0]*10, 0))
            means[s.sclass] = (m_aux[0] + (s.histograms[ind][0] - m_aux[0]) / (m_aux[2] + 1), s.histograms[ind][1][:-1], m_aux[2]+1)
        for k, v in means.items():
            plt.plot(v[1], v[0])
        plt.title(descriptor)
        plt.show()

    def ComputeDifferences(self, shape, weights = None):
        diffs = [s.Difference(shape) for s in self]
        if weights is None:
            weights = [1 / (len(self[0].histograms) + 5)] * (len(self[0].histograms) + 5)
        return np.argsort(np.dot(diffs, weights))

    def ANNSearch(self, shape, k):
        return self.ann.get_nns_by_item(np.argwhere(self.shapes == shape)[0][0], k)

    def ANNSearchV(self, vector, k):
        return self.ann.get_nns_by_vector(vector, k)

    def BuildANN(self, metric = 'angular', n_trees = 20):
        f = len(self[0].GetDescriptors())
        t = AnnoyIndex(f, metric)
        for i, s in enumerate(self):
            t.add_item(i, s.GetDescriptors())
        t.build(n_trees)
        self.ann = t

    def OptimizeWeightsSA(self, ann_rate = 1.1, max_its = 100, int_it = 300, temp = 300):
        wl = len(self[0].histograms)+4
        ncl = len(self.classes)
        ss = np.tile(self.sclass, (ncl,1)).T
        mat = [[s1.Difference(s2) for s2 in self] for s1 in self]

        def ComputeObjective(weights):
            wmat = np.argsort(np.dot(mat, weights), axis=0)[:,:ncl]
            cmat = [[self[c].sclass for c in r] for r in wmat]
            return np.sum(cmat == ss)
        
        def Neighbour(weights):
            pos = np.random.randint(0,wl)
            dr = np.random.choice([-1,1])
            dst = np.random.uniform(0.001,0.1)
            if np.random.randint(0,2):
                weights -= dr*dst/(wl-1)
                weights[pos] += dr*dst*(1+1/(wl-1))
            else:
                weights[pos] += dr*dst
                weights[np.random.randint(0,wl)] -= dr*dst
            return weights

        best = np.ones(wl)
        curr = best.copy()
        imps = []
        conv = False
        best_obj = curr_obj = ComputeObjective(curr)

        for _ in range(max_its):
            temp /= ann_rate
            print(f'New temperature: {temp}')
            print(f'Current: {curr} with {curr_obj}')
            for _ in range(int_it):
                imp = False
                aux = Neighbour(curr)
                aux_obj = ComputeObjective(curr)
                if np.exp(-(curr_obj-aux_obj)/temp) > np.random.uniform():
                    curr = aux
                    curr_obj = aux_obj
                    if curr_obj > best_obj:
                        print(f'Best updated from {best} to {curr} with {curr_obj}')
                        best = curr
                        best_obj = curr_obj
                        imp = True
                imps.append(imp)
            if temp < 0.2 and np.all(np.array(imps[-10:]) == False):
                break
        return best

    def OptimizeWeightsPerceptron(self, initial_weights = [1,1,1,1,1,1,1,1,1,1], min_it = 10, no_imp_it = 10, rate = 0.1, rate_change = 1):
        weights = [initial_weights]
        imps = []

        els = np.sum([1 for s in self if s.sclass == self[0].sclass])

        mat = np.array([[s1.Difference(s2) for s2 in self] for s1 in self])

        i = 0
        best_corr = 0
        best_weights = weights[0]
        while len(imps) < min_it or np.any(imps[-no_imp_it:]):
            print(f'Iteración #{i}')
            curr_corr = 0
            for j in range(len(self.shapes)):
                s = self[j]
                lst = np.argsort(np.dot(mat[j,:,:], weights[i]), axis=0)[:els]
                curr_corr += sum([1 for x in lst if self[x].sclass == s.sclass])
                weights[i] = weights[i] + np.sum([-1*np.array(s.SDifference(self[g]))*rate for g in lst if self[g].sclass != s.sclass], axis = 0)
            imps.append(curr_corr > best_corr)
            best_weights = weights[i] if curr_corr > best_corr else best_weights
            best_corr = max(curr_corr, best_corr)
            print(f'Best - {best_corr} Current - {curr_corr}')
            i+=1
            rate *= rate_change
            weights.append(weights[i-1])
        print(f'Best weights found: {best_weights}')
        return best_weights

    def ShowTSNEReduction(self):
        palette = np.array([    "#f16561", "#4c6397", "#36d58a", "#1f0947", "#63ace9", "#9e3c5a", 
                                "#092b6f", "#330268", "#5fcde9", "#91ce22", "#6b2063", "#246edd", 
                                "#7633ae", "#a796d4", "#017624", "#0d0320", "#cb28b2", "#02d0e7",
                                "#c47c4e"])
        fig,ax = plt.subplots()

        def update_annot(ind):
            pos = sc.get_offsets()[ind["ind"][0]]
            annot.xy = pos
            text = "{}, {}".format(self[ind["ind"][0]].name, self[ind["ind"][0]].sclass)
            annot.set_text(text)
            annot.get_bbox_patch().set_facecolor(palette[np.argwhere(self.classes== self[ind["ind"][0]].sclass)[0][0]])
            annot.get_bbox_patch().set_alpha(0.4)


        def hover(event):
            vis = annot.get_visible()
            if event.inaxes == ax:
                cont, ind = sc.contains(event)
                if cont:
                    update_annot(ind)
                    annot.set_visible(True)
                    fig.canvas.draw_idle()
                else:
                    if vis:
                        annot.set_visible(False)
                        fig.canvas.draw_idle()

        colour = palette[[np.argwhere(self.classes == s.sclass)[0][0] for s in self]]
        tsne = TSNE(n_components=2).fit_transform([s.GetDescriptors() for s in self])
        sc = plt.scatter(tsne[:,0], tsne[:,1], c = colour)

        annot = ax.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)
        fig.canvas.mpl_connect("motion_notify_event", hover)

        proxy = [plt.Rectangle((0,0),1,1,fc = c) for c in palette]
        plt.legend(proxy, self.classes)
        plt.show()

    def ShowTSNEReduction3D(self):
        import pylab
        from mpl_toolkits.mplot3d import Axes3D
        from mpl_toolkits.mplot3d import proj3d
        palette = np.array([    "#f16561", "#4c6397", "#36d58a", "#1f0947", "#63ace9", "#9e3c5a", 
                                "#092b6f", "#330268", "#5fcde9", "#91ce22", "#6b2063", "#246edd", 
                                "#7633ae", "#a796d4", "#017624", "#0d0320", "#cb28b2", "#02d0e7",
                                "#c47c4e"])
        fig = pylab.figure()
        ax = fig.add_subplot(111, projection = '3d')

        def update_annot(ind):
            pos = sc.get_offsets()[ind["ind"][0]]
            annot.xy = pos
            text = "{}, {}".format(self[ind["ind"][0]].name, self[ind["ind"][0]].sclass)
            annot.set_text(text)
            annot.get_bbox_patch().set_facecolor(palette[np.argwhere(self.classes == self[ind["ind"][0]].sclass)[0][0]])
            annot.get_bbox_patch().set_alpha(0.4)


        def hover(event):
            vis = annot.get_visible()
            if event.inaxes == ax:
                cont, ind = sc.contains(event)
                if cont:
                    update_annot(ind)
                    annot.set_visible(True)
                    fig.canvas.draw_idle()
                else:
                    if vis:
                        annot.set_visible(False)
                        fig.canvas.draw_idle()

        colour = palette[[np.argwhere(self.classes == s.sclass)[0][0] for s in self]]
        tsne = TSNE(n_components=3).fit_transform([s.GetDescriptors() for s in self])
        sc = ax.scatter(tsne[:,0], tsne[:,1], tsne[:,2], c = colour)

        annot = pylab.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)

        fig.canvas.mpl_connect("motion_notify_event", hover)

        proxy = [plt.Rectangle((0,0),1,1,fc = c) for c in palette]
        plt.legend(proxy, self.classes)
        plt.show()

    def ShowConfusionMatrix(self, stype = 'ANN', k = 20, weights = None):
        cm = np.zeros((len(self.classes), len(self.classes)))
        if stype == 'ANN':
            if not hasattr(self, 'ann'):
                self.BuildANN()
            pred = np.array([[self[p].sclass for p in self.ANNSearch(s, k)] for s in self])
        elif stype == 'weighted':
            if weights == None:
                weights = self.pc_ws
            pred = np.array([[self[p].sclass for p in self.ComputeDifferences(s, weights)[:k]] for s in self])
        else:
            pred = np.array([[self[p].sclass for p in self.ComputeDifferences(s)[:k]] for s in self])
        for i, c1 in enumerate(self.classes):
            aux = pred[self.sclass == c1]
            for j, c2 in enumerate(self.classes):
                cm[i, j] = np.sum(aux == c2)
        cm /= np.sum(cm[0])

        fig, ax = plt.subplots()
        im = ax.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
        ax.figure.colorbar(im, ax=ax)
        ax.set(xticks=np.arange(cm.shape[1]),
                yticks=np.arange(cm.shape[0]),
                xticklabels=self.classes, yticklabels=self.classes,
                title="Confusion matrix of LPSB querying for 20 elements",
                ylabel='Class',
                xlabel='Returned elements')

        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                    rotation_mode="anchor")

        fmt = '.2f'
        thresh = cm.max() / 2.
        for i in range(cm.shape[0]):
            for j in range(cm.shape[1]):
                ax.text(j, i, format(cm[i, j], fmt),
                        ha="center", va="center",
                        color="white" if cm[i, j] > thresh else "black")
        fig.tight_layout()

        plt.show()



    def createContour(self, vals, xticks, yticks, title="", xlabel="", ylabel = "", ncolours=11):
        levels = np.linspace(0,1,ncolours)
        cs = plt.contourf(vals, levels = levels)
        plt.xticks(range(len(xticks)), labels = xticks)
        plt.yticks(yticks)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        proxy = [plt.Rectangle((0,0),1,1,fc = pc.get_facecolor()[0]) for pc in cs.collections]
        plt.legend(proxy, levels, bbox_to_anchor=(1.1, 1))
        plt.title(title)
        plt.show()

    def ShowMetricGraphs(self, stype = 'ANN', k = 20, ncolours = 10, weights = None):

        def plot(vals, labels):
            plt.plot(vals)
            plt.title('Matthews Correlation Coefficient per class')
            plt.ylim((0.2,0.7))
            plt.xticks(range(len(labels)), labels = labels)
            plt.show()

        classes = np.unique([s.sclass for s in self])
        els = np.sum([1 for s in self if s.sclass == self[0].sclass])
        ppv = np.empty((len(classes), els)) 
        tnr = np.empty((len(classes), els))
        stts = np.zeros((len(classes), 4))
        c_count = {c:0 for c in classes}
        
        if stype == 'ANN':
            self.BuildANN()
        elif stype == 'weighted':
            if weights == None:
                weights = self.pc_ws
                
        for s in self:
            
            if stype == 'ANN':
                S = self.ANNSearch(s, k)
                N = [x for x in range(len(self.shapes)) if not x in S]
            elif stype == 'weighted':
                lst = self.ComputeDifferences(s, weights)
                S = lst[:k]
                N = lst[k:] 
            else:
                lst = self.ComputeDifferences(s)
                S = lst[:k]
                N = lst[k:]
            tp = sum([1 for x in S if self[x].sclass == s.sclass])
            tn = sum([1 for x in N if not self[x].sclass == s.sclass])

            cs = np.argwhere(classes==s.sclass)[0][0]
            ppv[cs, c_count[s.sclass]] = tp / len(S) if len(S) > 0 else 0
            tnr[cs, c_count[s.sclass]] = tn / (tn + len(S) - tp)
            stts[cs, :] += [tp, len(S)-tp, tn, len(N)-tn]
            c_count[s.sclass]+=1

        self.createContour(ppv.T, classes, range(0,20,5), title = 'PPV per class', ncolours = ncolours)
        self.createContour(tnr.T, classes, range(0,20,5), title = 'TNR per class', ncolours = ncolours)

        tps = stts[:,0]
        fps = stts[:,1]
        tns = stts[:,2]
        fns = stts[:,3]
        mcc = np.multiply(np.multiply(tps, tns)+np.multiply(fps, fns), 1/np.sqrt(np.multiply(np.multiply(np.multiply(tps+fps, tps+fns),tns+fps),tns+fns)))
        plot(mcc.T, classes)

    def ShowGrowthPlot(self, stype='ANN', k=20, weights = None):
        def addDict(dct, pos, new):
            val = dct.get(pos, [])
            val.append(new)
            dct[pos] = val

        ppv = np.empty(shape=(len(self.shapes),len(self.shapes)))
        palette = ["#f16561", "#4c6397", "#36d58a", "#1f0947", "#63ace9", "#9e3c5a", "#092b6f", "#330268", "#5fcde9", "#91ce22", "#6b2063", "#246edd", "#7633ae", "#a796d4", "#017624", "#0d0320", "#cb28b2", "#02d0e7","#c47c4e"]
        ppv = {}
        if stype == 'ANN':
            self.BuildANN()
        elif stype == 'weighted':
            if weights == None:
                weights = self.pc_ws
        for i, s in enumerate(self):
            if stype == 'ANN':
                lst = self.ANNSearch(s, k)
            elif stype == 'weighted':
                lst = self.ComputeDifferences(s, weights)[:k]
            else:
                lst = self.ComputeDifferences(s)[:k]
            
            vals = np.array(list(accumulate(lst, lambda x,y: x + (1 if self[y].sclass == s.sclass else 0),initial=0)))
            addDict(ppv, s.sclass, vals)

        means = {u:np.mean(v, axis=0)/k for u, v in ppv.items()}
        c_it = iter(palette)
        for v in means.values():
            plt.plot(v, c = next(c_it))
        plt.plot(np.array(range(k+1))/k, 'k')
        proxy = [plt.Rectangle((0,0),1,1,fc = c) for c in palette]
        plt.legend(proxy, means.keys())
        plt.ylabel("Percentage of meshes of the same class in the resulting set")
        plt.xlabel("Size of the resulting set")
        plt.xticks(list(range(1,k+1)))
        plt.show()

    def CompareMetricsNTrees(self, ncolours = 11):
        metrics = ["angular", "euclidean", "manhattan", "hamming", "dot"]
        sz_class = {c:np.sum(self.sclass==c) for c in self.classes}

        ppv = dict()
        times = dict()
        def addDict(dct, pos, new):
            val = dct.get(pos, [])
            val.append(new)
            dct[pos] = val

        for m in metrics:
            print(m)
            ppv_m = {}
            for n in range(len(self.shapes)):
                ppv_mn = {}
                ini = time.time()
                self.BuildANN(m, n)
                for s in self:
                    addDict(ppv_mn, s.sclass, np.sum([1 for k in self.ANNSearch(s, sz_class[s.sclass]) if self[k].sclass == s.sclass])/sz_class[s.sclass])
                times[m,n] = time.time() - ini
                ppv_m[n] = {k: np.mean(v) for k,v in ppv_mn.items()}
            ppv[m] = ppv_m

        for m, v in ppv.items():
            self.createContour( np.array([list(v.values()) for v in v.values()]), 
                                xticks = self.classes, yticks = range(0,len(self.shapes)+5,10), 
                                title = m, xlabel = 'Metric', ylabel = 'Number of trees', 
                                ncolours = ncolours)


    def Save(self, name):
        if hasattr(self, 'ann'):
            del self.ann
        np.save(name, [self])

    def __getitem__(self, key):
        return self.shapes[key]

def RedoDB(path, mdir = "./meshes", *, resolution = (660, 405), save = None):
    db = ShapeDataBase(path, mdir = mdir)
    db.RefineNormalize()
    db.CalcDescriptors()
    if save:
        db.Save(save)
    return db
