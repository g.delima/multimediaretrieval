from op import Shape, ShapeDataBase, RedoDB
import numpy as np

#db = RedoDB("./LabeledDB", mdir = "./MeshDir", save='LDB')
db = np.load("LDB.npy", allow_pickle=True)[0]

db.CompareMetricsNTrees(ncolours = 11)

db.ShowMetricGraphs(    stype = 'ANN',      k = 20, ncolours = 11)
db.ShowMetricGraphs(    stype = 'weighted', k = 20, ncolours = 11)
db.ShowMetricGraphs(    stype = 'normal',   k = 20, ncolours = 11)
db.ShowMetricGraphs(    stype = 'ANN',      k = 5,  ncolours = 11)
db.ShowMetricGraphs(    stype = 'weighted', k = 5,  ncolours = 11)
db.ShowMetricGraphs(    stype = 'normal',   k = 5,  ncolours = 11)

db.ShowGrowthPlot(      stype = 'ANN',      k = 20)
db.ShowGrowthPlot(      stype = 'weighted', k = 20)
db.ShowGrowthPlot(      stype = 'normal',   k = 20)
db.ShowGrowthPlot(      stype = 'ANN',      k = 5)
db.ShowGrowthPlot(      stype = 'weighted', k = 5)
db.ShowGrowthPlot(      stype = 'normal',   k = 5)

db.ShowConfusionMatrix( stype = 'ANN',      k = 20)
db.ShowConfusionMatrix( stype = 'weighted', k = 20)
db.ShowConfusionMatrix( stype = 'normal',   k = 20)