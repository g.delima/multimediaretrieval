The libraries required to run the code are specified in the "requirements.txt" file. 
Running "pip install -r requirements.txt" should install any of those libraries missing.
I have had some trouble using pip to install certain libraries, so I have added the .whl file of those libraries in the dependencies folder.  
This files are installed equally through pip, using the "pip install filename.whl" command.
The dependencies folder is organised in 4 subfolders, each containing the .whl corresponding to different python versions. 
I have included both 3.7 and 3.8, for 64 bytes builds.
Just to be safe, I have tried to add all libraries for the 3.7 - 64 bytes build. This is the only with all .whl so as to not take too much space.
I use Windows, if for some reason these files are not compatible with your OS distribution, an exhaustive list of .whl files can be found at https://www.lfd.uci.edu/~gohlke/pythonlibs/. 
I used Python 3.8 when writing this code. I tried to making it compatible with at least Python 3.7, but I may have missed something.

The code is organised in 3 files:
    - op.py: contains the guts of the code. The two main classes Shape and ShapeDataBase are defined here. It contains all methods.
    - gui.py: contains the code for the GUI.
    - replicate_graphs.py: contains a series of commands to replicate the figures included in the "Evaluation" section of the report.

Once all the libraries are installed, running gui.py or replicate_graphs.py should be straightforward.

There is an additional file included: "contour_script3d.py". This is the script I used to generate figure 3.1 in the report. It is however not runnable, as it requires some attributes in the database that are now deleted after building it, for memory reasons.
This file is only included for the sake of completion, and reference as to how I obtained it. The result is not reproduceable without changing the code of the Shape and ShapeDataBase classes, however. 

In the projec there are two .npy files: LDB.npy and gui-database.npy. Both contain the same database, however gui-database.npy includes the preloaded image of each mesh. gui-database.npy is the default name that is given to every database saved from the GUI.

When creating a new database, the progress of the database building is displayed in the terminal. 

GUI Usage:
    - Load preprocessed database by clicking on the second "Open File" button, or create a new database by selecting a folder clicking on the "Open folder" button. 
        It is also possible to use the drag-and-drop field right next to them.
    - Once the database is loaded into the system, select a mesh by clicking on a class on the leftmost column, and then a mesh in the rightmost one; or load an external mesh by clicking on the first "Open File" button.
    - This should load an image of the mesh in the upper black box, along with the histograms for its descriptors in the upper-right-most black box.
    - Visualise the 3D mesh by clicking on "Show 3D view", or query for the n most similar shapes by using one of the two search buttons.
    - The middle-leftmost buttons allow to change the weights of the descriptors in the query process, and see a breakdown of the distances between the two meshes.
    - Use the left and right (or click to the left and right of the result image) to move over the different n results.
    - Click on the lower-leftmost buttons to visualise the reduction of feature vectors of the shapes in the database to 2 and 3 dimensions.